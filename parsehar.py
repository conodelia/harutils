"""Reads a har file from the filesystem, converts to CSV, then dumps to
stdout.
"""
import time
import argparse
import json
import requests
from urllib.parse import urlparse
from datetime import datetime
from datetime import timedelta
import configparser
import logging

logging.basicConfig(filename='parsehar-app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

config = configparser.ConfigParser()
config.read("parsehar.properties")
CfgTraceheader = config.get('DEFAULT', 'TraceHeader')
CfgAPIkey = config.get('DEFAULT', 'APIKey')
CfgBackendURL = config.get('DEFAULT', 'BackendURL')


def findHeader(headers, header_name):
    Value = "None"
    for header in headers:
        headerName = header['name']
        
        if headerName == header_name:
            header_value = header['value']
            Value =  header_value


    return Value
        
def getTrace(trace_id):
    #print "trace_id "+trace_id
    urlrequest = "https://api.us1.signalfx.com/v2/apm/trace/"+trace_id+"/latest"

    payload = ""
    headers = {
        'X-SF-TOKEN': CfgAPIkey,
        'Content-Type': "application/json",
        'cache-control': "no-cache"
        #'Postman-Token': "9c7f33f2-a78b-4699-bf0d-d2f34de968bb"
        }
    response = requests.request("GET", urlrequest, data=payload, headers=headers)

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        #print "Errorz: " + str(e)
        logging.error("trace id: "+trace_id+" resulted in "+str(e))
        return 0, 0, "None"

    
    pz_jvm_elapsed_time = 0
    be_max_elapsed_time = 0
    pzjvm_list = []
    be_list = []
    be_time_list = []

    duration_micros = 0
    span_kind = "None"
    span_url = "None"
    list_of_calls = ""
    http_status_code=""

    
    j = 0

    for span in json.loads(response.text):
        
        pzjvm_list.append(span['durationMicros'])
      
        if 'span.kind' in span['tags']: 
            span_kind = span['tags']['span.kind']
            if 'http.url' in span['tags']: 
                duration_micros =  span['durationMicros']
                span_url = span['tags']['http.url']
                substring = CfgBackendURL
                if substring in span_url:
                    """print("Found!")
                    print span_url
                    print duration_micros                    
                    """
                    if 'http.status_code' in span['tags']:
                        http_status_code = span['tags']['http.status_code'] 

                    start_date = datetime.strptime(span['startTime'], "%Y-%m-%dT%H:%M:%S.%fZ")
                    end_date = start_date + timedelta(milliseconds=span['durationMicros']/1000)
                    delta_time = (end_date - start_date) 
                    be_list.append(duration_micros) 
                    be_time_list.append(start_date) 
                    be_time_list.append(end_date)
                    #list_of_calls += span_url.split("Perf01",1)[1] +";"+str(start_date)+";"+str(duration_micros/1000)+";"+http_status_code+"|**|"
                    list_of_calls += span_url+";"+str(start_date)+";"+str(duration_micros/1000)+";"+http_status_code+"|**|"
                   
                                                        
            else:
                span_url = "None"
                http_status_code=""
        else:
            span_kind = "None"
        j = j + 1

    

    
    max_be_list = 0
    max_pzjvm_list = 0

    if be_time_list:
        start_time = min(be_time_list)
        #print ("min time ", start_time)
        end_time = max(be_time_list)
        #print ("end time: ", end_time)
        delta_time = (end_time - start_time) 
        #print ("delta time all calls", delta_time)
        
        millisec = end_time.timestamp()*1000 - start_time.timestamp()*1000
        #print ("millisec delta: ",millisec)

        max_be_list = millisec*1000


        #print ("delta time in ms: ",delta_time.timestamp() * 1000)

    if pzjvm_list:
        max_pzjvm_list = max(pzjvm_list)


    #print ("zzzzzz1: "+str(max_pzjvm_list)+""+str(max_be_list)+" "+list_of_calls)
    return max_pzjvm_list, max_be_list, list_of_calls


def main(args):

    
    harfile_path = args.harfile[0]
    traceid_flag = args.traceid_flag

   
    #pz_jvm_elapsed_time, be_max_elapsed_time, list_of_calls = getTrace("4d994f5cf08747a0950db7a634db33a4")
    #print str(pz_jvm_elapsed_time) + " " + str(be_max_elapsed_time)

    """Reads a har file from the filesystem, converts to CSV, then dumps to
    stdout.
    """
    harfile = open(harfile_path)
    harfile_json = json.loads(harfile.read())    
    
    print ("id,started_date_time,start_date_ms,end_date_ms,server_ipaddress,url,urlparts.hostname,res_content_size(b),res_content_compression(b),res_size_bytes(b),res_header_size(b),res_transfer_size(b),mimetype,trace_id,time_ms,timings_blocked_ms,timings_dns_ms,timings_ssl_ms,timings_connect_ms,timings_send_ms,timings_wait_ms,timings_receive_ms,timings_blk_queue_ms,from_cache,req_cache_control,res_cache_control,res_pragma,res_expires,res_cf_cache_status,res_expect_ct,res_server,res_cf_ray,res_date,res_last_modified,res_etag,res_x_cache,res_via,res_x_amz_cf_pop,res_x_amz_cf_id,res_age,pz_jvm_elapsed_time_ms,be_max_elapsed_time_ms,pz_jvm_wo_be_ms,spa_to_pz_jvm_ms,endpoint-starttime-duration_ms-httpstatuscode")
    
    i = 0

    for entry in harfile_json['log']['entries']:

        i = i + 1
        url = entry['request']['url']
        urlparts = urlparse(entry['request']['url'])

        content_size = entry['response']['content']['size']
        content_compression = 'None'
        if 'compression' in entry['response']['content']:
            content_compression = entry['response']['content']['compression']

        
        size_bytes = entry['response']['bodySize']
        header_size = entry['response']['headersSize']
        transfer_size = entry['response']['_transferSize']

        
        headers = entry['response']['headers']
        req_headers = entry['request']['headers']

        resource_type = entry['_resourceType']
  
     
        trace_id = findHeader(headers,CfgTraceheader)
        #trace_id = findHeader(headers,"trace-id")

        started_date_time = entry['startedDateTime']

        

        

        server_ipaddress = entry['serverIPAddress']

        rtime = entry['time']
        timings_blocked = entry['timings']['blocked']
        timings_dns = entry['timings']['dns']
        timings_ssl = entry['timings']['ssl']
        timings_connect = entry['timings']['connect']
        timings_send = entry['timings']['send']
        timings_wait = entry['timings']['wait']
        timings_receive = entry['timings']['receive']
        timings_blk_queue = entry['timings']['_blocked_queueing']

        start_date_ms = (datetime.strptime(started_date_time, "%Y-%m-%dT%H:%M:%S.%fZ")).timestamp()*1000
        end_date_ms = start_date_ms + rtime
  

        size_kilobytes = float(entry['response']['bodySize'])/1024

        url = entry['request']['url']
        mimetype = 'unknown'
        if 'mimeType' in entry['response']['content']:
            mimetype = entry['response']['content']['mimeType']


        size_bytes = entry['response']['bodySize']

        #cache settings
        if '_fromCache' in entry:
            from_cache = entry['_fromCache']
        else:
            from_cache = "None"


        cache_Control = findHeader(headers,"Cache-Control")            
        pragma = findHeader(headers,"Pragma")  
        expires = findHeader(headers,"Expires")
        cf_cache_status = findHeader(headers,"CF-Cache-Status") 
        expect_ct = findHeader(headers,"Expect-CT") 
        server = findHeader(headers,"Server")
        cf_ray = findHeader(headers,"CF-RAY")

        res_date = findHeader(headers,"Date")
        last_modified = findHeader(headers,"Last-Modified")
        etag = findHeader(headers,"ETag")
        x_cache = findHeader(headers,"X-Cache")
        via = findHeader(headers,"Via")
        x_amz_cf_pop = findHeader(headers,"X-Amz-Cf-Pop")
        x_amz_cf_id = findHeader(headers,"X-Amz-Cf-Id")
        age = findHeader(headers,"Age")


       


        req_cache_control = findHeader(req_headers,"Cache-Control")

        pz_jvm_elapsed_time = 0
        be_max_elapsed_time = 0
        list_of_calls = "None"
        

        
        if traceid_flag == "true":
            if trace_id != "None":
                pz_jvm_elapsed_time, be_max_elapsed_time, list_of_calls = getTrace(trace_id)
                time.sleep(5)
                     

        spa_to_pz_jvm = 0
        #calculate spa_to_pz_jvm
        if pz_jvm_elapsed_time != 0:
            spa_to_pz_jvm = rtime - pz_jvm_elapsed_time/1000

        pz_jvm_wo_be = 0
        if pz_jvm_elapsed_time != 0 and be_max_elapsed_time != 0:
        #calculate pz_jvm_wo_be (elapsed time of pz jvm minus the be calls) 
            pz_jvm_wo_be = pz_jvm_elapsed_time/1000 - be_max_elapsed_time/1000
         

        
        print ('%s,%s,"%.0f","%.0f","%s","%s",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s",%s,%s,%s,%s,"%s"' % (i, started_date_time, start_date_ms, end_date_ms, server_ipaddress, url, urlparts.hostname, 
                                    content_size, content_compression, size_bytes, 
                                    header_size, transfer_size, mimetype, trace_id, rtime, timings_blocked,
                                       timings_dns, timings_ssl, timings_connect, timings_send, timings_wait,
                                       timings_receive, timings_blk_queue, 
                                       from_cache, req_cache_control, cache_Control, pragma, expires, cf_cache_status, expect_ct, server, cf_ray,
                                       res_date, last_modified, etag, x_cache, via, x_amz_cf_pop, x_amz_cf_id, age,   
                                       pz_jvm_elapsed_time/1000, be_max_elapsed_time/1000,
                                       pz_jvm_wo_be, spa_to_pz_jvm,list_of_calls ))
       

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(
        prog='parsehar',
        description='Parse .har files into comma separated values (csv).')
    argparser.add_argument('harfile', type=str, nargs=1,
                        help='path to harfile to be processed.')

    argparser.add_argument('-t', action='store_const', dest='traceid_flag',
                    const='true',
                    help='Look up and extract Trace Ids from Splunk APM')

    #results = argparser.parse_args()
   
    args = argparser.parse_args()
    main(args)
