import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import logging
import argparse
import configparser
from collections import Counter
from decimal import Decimal
  
logging.basicConfig(filename='haranalyzer-app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

config = configparser.ConfigParser()


def main(args):

    
    harfile_path = args.parsedharfile[0]

    # Initialize the lists for X and Y
    data = pd.read_csv(harfile_path,index_col="id", dtype={'start_date_ms':object, 'end_date_ms':object})
    
    df = pd.DataFrame(data)
    
    print ("")
    print("*****************************************")
    print("Top 20 longest duration browser responses")
    print("*****************************************")
    print (df.nlargest(20,['time_ms'],keep="first")[[ 'url', 'time_ms','pz_jvm_elapsed_time_ms','be_max_elapsed_time_ms','pz_jvm_wo_be_ms','spa_to_pz_jvm_ms']])

    print ("")
    print("*****************************************")
    print("Top 20 longest duration PZ JVM responses")
    print("*****************************************")
    print (df.nlargest(20,['pz_jvm_elapsed_time_ms'],keep="first")[[ 'url', 'pz_jvm_elapsed_time_ms','time_ms','be_max_elapsed_time_ms','pz_jvm_wo_be_ms','spa_to_pz_jvm_ms']])
   
    print ("")
    print("*****************************************")
    print("Top 20 longest duration Backend responses")
    print("*****************************************")
    print (df.nlargest(20,['be_max_elapsed_time_ms'],keep="first")[[ 'url', 'be_max_elapsed_time_ms', 'time_ms','pz_jvm_elapsed_time_ms','pz_jvm_wo_be_ms','spa_to_pz_jvm_ms']])


    print ("")
    print("***********************************************")
    print("Top 20 longest PZ JVM responses without backend")
    print("***********************************************")
    print (df.nlargest(20,['pz_jvm_wo_be_ms'],keep="first")[[ 'url', 'pz_jvm_wo_be_ms','time_ms','pz_jvm_elapsed_time_ms','be_max_elapsed_time_ms','spa_to_pz_jvm_ms']])
    

    print ("")
    print("********************************")
    print("Top 20 largest response payloads")
    print("********************************")
    print (df.nlargest(20,['res_transfer_size(b)'],keep="first")[[ 'url','res_transfer_size(b)']])


    print ("")
    print("***********************************************************")
    print("Top 20 longest PZ Edge response times with no backend calls")
    print("***********************************************************")
    no_backend = (df['pz_jvm_elapsed_time_ms'] != 0) & (df['be_max_elapsed_time_ms'] == 0)
    print (   df[no_backend][['url','pz_jvm_elapsed_time_ms']].nlargest(20,['pz_jvm_elapsed_time_ms'],keep="first")    )

    total_records = len(df.index)
    prev_elapsed_call= []
    
    for index, row in df.iterrows():
        #print(index, row['start_date_ms'], row['end_date_ms'], row['url'])
        if (index == 1):
            #print(index, row['url'], 0)
            prev_elapsed_call= [0]
        else:
            #print( '%s, %.0f' %  (str(index), Decimal(data.iloc[index-2]['end_date_ms']) - Decimal(row['start_date_ms']) )  )
            prev_elapsed_call.append(  Decimal(data.iloc[index-2]['end_date_ms']) - Decimal(row['start_date_ms'])  )            
            #print ("")
            #print ("***")

    df['elapsed_time_from_prev_call_ms'] =  prev_elapsed_call


    print ("")
    print("******************************************")
    print("Top 20 longest delays between previous call")
    print("******************************************")
    print (df.sort_values(['elapsed_time_from_prev_call_ms'],ascending=False).head(20)[['url','elapsed_time_from_prev_call_ms']])

 
    
    print ("")
    print("*****************************************")
    print("Distribution of caching resource servers")
    print("*****************************************")
    print(Counter(df['res_server']))


    print ("")
    print("*****************************************")
    print("Distribution of response cache-control")
    print("*****************************************")
    print(Counter(df['res_cache_control']))

    print ("")
    print("*****************************************")
    print("Distribution of browser caching")
    print("*****************************************")
    print(Counter(df['from_cache']))

    print ("")
    print("*****************************************")
    print("Distribution SPA endpoints")
    print("*****************************************")
    print(Counter(df['urlparts.hostname']))    
        

    print ("Total Records: ",total_records)


    print(df.dtypes)

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(
        prog='haranalyzer',
        description='Analyze parsed har output.')
    argparser.add_argument('parsedharfile', type=str, nargs=1,
                        help='path to parsed har file to be processed.')


   
    args = argparser.parse_args()
    main(args)