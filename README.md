# README #

This repo contains utilities to help parse and analyze HAR files.

### Description of Utilities  ###

* parsehar.py - parses a har file and generates a csv and also enriches the standard har metrics with additional trace data from SPLUNK 
* haranalyzer.py - takes the output of parsehar.py and generates reports   

### How do I get set up? ###

The following dependencies must be installed

* Python 3 runtime
The following Python libraries

* matplotlib
* numpy
* pandas

###Installing the python libraries###

> pip3 install pandas
> pip3 install numpy
> pip3 install matplotlib

### Update config file ###

parsehar.py takes in a property file named parsehar.properties. It will also generate an error log named parsehar-app.log for any SPLUNK APM API errors


[DEFAULT]
TraceHeader = trace-id # no need to change this for PZ
APIKey = AZ83FZda231 # UPDATE THIS TO YOUR API KEY IN SPLUNK APM ACCOUNT PROFILE! 
BackendURL = apiservices.cooperators.ca # no need to change this for PZ

### How to Run ###

* Generate the HAR CSV

> python3 parsehar.py coop1.har -t > coop1-har.csv

-t flag enriches the csv with SPLUNK APM data
output is to stdout unless otherwise redirected
 
* Generate the HAR analysis report 

> python3 haranalyzer.py coop1-har.csv

output is to stdout unless otherwise redirected
